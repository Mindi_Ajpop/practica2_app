const { Pool } = require("pg");
const dotenv = require("dotenv").config();

const pool = new Pool({
	user: process.env.USERB,
	host: process.env.HOSTB,
	database: process.env.DATABASEB,
	password: process.env.PASSWORDB,
	port: process.env.PORTB,
});

module.exports = pool;
