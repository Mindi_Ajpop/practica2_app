const dotenv = require("dotenv").config();
const ordenes = require("../dbApis/ordenes");

async function post(req, res) {
	let body = req.body;
	let orden = {
		idUsuario: req.params.id ? parseInt(req.params.id, 10) : 0,
		idTipoEntrega: body.idTipoEntrega,
		nombre: body.nombre,
		fecha: body.fecha,
		direccion: body.direccion, 
		nit: body.nit,
		productos: body.productos,
		id_admin: body.id_admin,
	};
	try {
		const createO = await ordenes.create(orden);

		if (createO.tipo == "OK" && createO.estado == 201) {
			res.status(createO.estado).json({
				mensaje: createO.message,
				idOrden: createO.idOrden,
			});
		} else {
			res.status(createO.estado).json({
				mensaje: createO.message,
				stack: createO.stack,
			});
		}
	} catch (e) {
		console.log(e.message);

		let E = {
			nivel: "Sistema",
			origen: "Servicio Ordenes",
			descripcion: "", //Solo si es error de usuario
			entrada: "", //Solo si es error de usuario
			mensaje: e.message, //Solo si es error de sistema
			stacktrace: e.stack, //Solo si es error de sistema
		};
		await InsertError(E);

		res.status(500).json({
			mensaje: "Error no controlado",
			stack: createO.stack,
		});
	}
}
module.exports.post = post;

async function put(req, res) {
	let body = req.body;

	/*
        ESTADOS DE ORDEN
            1 - Nueva Orden
            2 - En Preparacion
            3 - En Camino
            4 - Entregada
            5 - Cancelada
            6 - Pagada
    */
	let orden = {
		idUsuario: parseInt(req.params.id, 10),
		tipoUsuario: body.tipoUsuario,
		idOrden: parseInt(body.idOrden, 10),
		nuevoEstado: parseInt(body.nuevoEstado),
	};

	console.log(orden);

	try {
		let delOrden = await ordenes.update(orden);
		console.log(delOrden);

		if (delOrden.tipo == "OK" && delOrden.estado == 201) {
			res.status(delOrden.estado).json({
				mensaje: delOrden.message,
				idOrden: delOrden.idOrden,
				nuevoEstado: delOrden.estadoOrden,
			});
		} else {
			res.status(delOrden.estado).json({
				mensaje: delOrden.message,
				stack: delOrden.stack,
			});
		}
	} catch (e) {
		console.log(e.message);

		let E = {
			nivel: "Sistema",
			origen: "Servicio Ordenes",
			descripcion: "", //Solo si es error de usuario
			entrada: "", //Solo si es error de usuario
			mensaje: e.message, //Solo si es error de sistema
			stacktrace: e.stack, //Solo si es error de sistema
		};
		await InsertError(E);

		res.status(500).json({
			mensaje: "Error no controlado",
			stack: createO.stack,
		});
	}
}
module.exports.put = put;

async function getOrdenes(req, res) {
	let idUsuario = parseInt(req.params.id, 10);

	try {
		getOs = await ordenes.getOrdenes(idUsuario, -1, -1);
		res.status(200).json(getOs);
	} catch (error) {
		console.log(e.message);

		let E = {
			nivel: "Sistema",
			origen: "Servicio Ordenes",
			descripcion: "", //Solo si es error de usuario
			entrada: "", //Solo si es error de usuario
			mensaje: e.message, //Solo si es error de sistema
			stacktrace: e.stack, //Solo si es error de sistema
		};
		await InsertError(E);

		res.status(500).json({
			mensaje: "Error no controlado",
			stack: createO.stack,
		});
	}
}
module.exports.getOrdenes = getOrdenes;

async function getOrden(req, res) {
	let idUsuario = parseInt(req.params.id, 10);
	let idOrden = parseInt(req.params.idOrden, 10);

	try {
		getOs = await ordenes.getOrdenes(idUsuario, idOrden, -1);
		res.status(200).json(getOs);
	} catch (error) {
		console.log(e.message);

		let E = {
			nivel: "Sistema",
			origen: "Servicio Ordenes",
			descripcion: "", //Solo si es error de usuario
			entrada: "", //Solo si es error de usuario
			mensaje: e.message, //Solo si es error de sistema
			stacktrace: e.stack, //Solo si es error de sistema
		};
		await InsertError(E);

		res.status(500).json({
			mensaje: "Error no controlado",
			stack: createO.stack,
		});
	}
}
module.exports.getOrden = getOrden;

async function get(req, res) {
	let idUsuario = parseInt(req.params.idAdmin);
	try {
		getOs = await ordenes.getOrdenes(-1, -1, idUsuario);
		res.status(200).json(getOs);
	} catch (error) {
		console.log(e.message);

		let E = {
			nivel: "Sistema",
			origen: "Servicio Ordenes",
			descripcion: "", //Solo si es error de usuario
			entrada: "", //Solo si es error de usuario
			mensaje: e.message, //Solo si es error de sistema
			stacktrace: e.stack, //Solo si es error de sistema
		};
		await InsertError(E);

		res.status(500).json({
			mensaje: "Error no controlado",
			stack: createO.stack,
		});
	}
}
module.exports.get = get;
