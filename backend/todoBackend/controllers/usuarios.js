const dotenv = require("dotenv").config();
const usuarios = require("../dbApis/usuarios");
const pool = require("../config/database");
const md5 = require("md5");

const jwt = require("jsonwebtoken");

const TOKEN_SECRET = process.env.SECRET_KEY;

async function post(req, res, next) {
	let user = {
		nombre: req.body.nombre,
		apellido: req.body.apellido,
		correo: req.body.correo,
		dpi: req.body.dpi,
		direccion: req.body.direccion,
		password: req.body.password,
		tipo: req.body.tipo,
	};

	try {
		createU = await usuarios.create(user);

		if (createU == true) {
			//SI hay cambios en bd
			let Logg = {
				tipo: "Insertar Usuario",
				nombre: user.nombre,
				nivel: user.tipo == 1 ? "Administrador" : "Cliente",
				origen: "Servicio Usuarios",
				informacion: JSON.stringify({
					mensaje:
						"Insertando datos de usuario tipo: " +
						(user.tipo == 1 ? "Administrador" : "cliente"),
					data: {
						id: user.id,
						nombre: user.nombre,
						apellido: user.apellido,
						correo: user.correo,
						dpi: user.dpi,
						direccion: user.direccion,
						tipo: user.tipo,
					},
				}),
			};

			await usuarios.InsertLog(Logg);

			res.status(201).json({
				mensaje: "Usuario creado con exito.",
				usuario: user,
			});
		} else {
			//Error con los datos de consulta
			let E = {
				nivel: "Sistema",
				origen: "Servicio Usuarios",
				descripcion: "Error Insert",
				entrada: "Usuario " + user.correo,
				mensaje: createU,
				stacktrace: "",
			};

			await usuarios.InsertError(E);

			res.status(422).json({
				mensaje: "DPI y/o Correo ya existe(n)",
			});
		}
	} catch (err) {
		let E = {
			nivel: "Sistema",
			origen: "Servicio Usuarios",
			descripcion: "Error Insert",
			entrada: "Usuario " + user.correo,
			mensaje: err.message,
			stacktrace: err.stack,
		};
		await usuarios.InsertError(E);

		res.status(500).json({
			mensaje: err.message,
		});
	}
}
module.exports.post = post;

async function get(req, res, next) {
	try {
		let id = parseInt(req.params.id, 10);

		if (id) {
			await pool.query(
				"SELECT * FROM usuario WHERE id_usuario = " + id,
				(err, result) => {
					if (err) throw err;
					if (result.rowsCount != 0) {
						res.status(200).json(result.rows);
					} else {
						res.status(409).json(result.rows);
					}
				}
			);
		} else {
			await pool.query(
				"SELECT * FROM USUARIO ORDER BY id_usuario ASC",
				(err, result) => {
					if (err) throw err;
					res.status(200).json(result.rows);
				}
			);
		}
	} catch (err) {
		next(err);
	}
}
module.exports.get = get;

async function put(req, res, next) {
	let user = {
		nombre: req.body.nombre,
		apellido: req.body.apellido,
		correo: req.body.correo,
		dpi: req.body.dpi,
		direccion: req.body.direccion,
		password: req.body.password,
		id: parseInt(req.params.id, 10),
		tipo: parseInt(req.body.tipo),
	};

	try {
		updateU = await usuarios.update(user);

		/*
            Verificaciones
                True = Se ralizo el query, Si hay cambios en BD!
                False = Se realizo el query, pero no hubo cambios en BD (el id era erroneo)
                Otros: error de datos dentro de la consulta (dpi repetido, correo repetido, :v)
        */
		if (updateU == true) {
			//SI hay cambios en bd
			let Logg = {
				tipo: "Edicion",
				nombre: user.nombre,
				usuario: user.id,
				nivel: user.tipo == 1 ? "Administrador" : "Cliente",
				origen: "Servicio Usuarios",
				informacion: JSON.stringify({
					mensaje:
						"Actualizacion de datos de " +
						(user.tipo == 1 ? "Administrador" : "cliente"),
					newData: {
						id: user.id,
						nombre: user.nombre,
						apellido: user.apellido,
						correo: user.correo,
						dpi: user.dpi,
						direccion: user.direccion,
					},
				}),
			};
			await usuarios.InsertLog(Logg);

			res.status(201).json({
				mensaje: "Datos Actualizados",
				datosNuevos: user,
			});
		} else if (updateU == false) {
			//El id no existe, no hay cambios en BD
			res.status(201).json({
				mensaje: "Id no encontrado, se actualizo 0 filas",
			});
		} else {
			//Error con los datos de consulta
			let E = {
				nivel: "Usuario",
				origen: "Servicio Usuarios",
				descripcion: "DPI y/o Correo ya existe", //Solo si es usuario
				entrada: JSON.stringify({
					entrada: user.correo,
					dpi: user.dpi,
				}),
				mensaje: "", //Solo si es sistema
				stacktrace: "", //Solo si es sistema
			};
			await usuarios.InsertError(E);

			//422, Peticion correcta, pero con errores semanticos
			res.status(409).json({
				mensaje: "DPI y/o Correo ya existe(n)",
			});
		}
	} catch (err) {
		let E = {
			nivel: "Sistema",
			origen: "Servicio Usuarios",
			descripcion: "",
			entrada: "",
			mensaje: err.message,
			stacktrace: err.stack,
		};
		await usuarios.InsertError(E);

		res.status(404).json({
			mensaje: err.message,
		});
	}
}
module.exports.put = put;

function authenticateToken(req, res, next) {
	const authHeader = req.headers["authorization"];
	const token = authHeader && authHeader.split(" ")[1];
	if (token == null) return res.sendStatus(401);
	jwt.verify(token, TOKEN_SECRET, (err, user) => {
		if (err) return res.sendStatus(403);
		req.user = user;
		next();
	});
}

function generateAccessToken(Username) {
	return jwt.sign(Username, TOKEN_SECRET);
}

function authenticateToken(req, res, next) {
	const authHeader = req.headers["authorization"];
	const token = authHeader && authHeader.split(" ")[1];
	if (token == null) return res.sendStatus(401);
	jwt.verify(token, TOKEN_SECRET, (err, user) => {
		if (err) return res.sendStatus(403);
		req.user = user;
		next();
	});
}

function generateAccessToken(Username) {
	return jwt.sign(Username, TOKEN_SECRET);
}

async function login(req, res, next) {
	const { Username, Password } = req.body;
	let user = {
		Correo: Username,
		Password: Password,
	};

	try {
		getU = await usuarios.getUser(user);
		if (getU != false) {
			// si existe usuario con ese correo
			//verificar contraseña

			if (getU.password == user.Password) {
				let nivel1 = "";
				if (getU.tipo_usuario == 1) {
					nivel1 = "Administrador";
				} else if (getU.tipo_usuario == 2) {
					nivel1 = "Cliente";
				}
				let Logg = {
					tipo: "inicio Sesion",
					nombre: getU.nombre,
					usuario: getU.id_usuario,
					nivel: nivel1,
					origen: "Servicio Usuarios",
					informacion: "Inicio sesión  de " + nivel1,
				};
				r2 = await usuarios.InsertLog(Logg);
				res.status(200).json({
					status: 200,
					token: generateAccessToken(user.Correo),
					id: getU.id_usuario,
					tipo: getU.tipo_usuario,
				});
			} else {
				let E = {
					nivel: "usuario",
					origen: "Servicio Usuarios",
					descripcion: "Contraseña incorrecta",
					entrada: "contraseña",
					mensaje: "Contraseña incorrecta",
					stacktrace: "",
				};
				r2 = await usuarios.InsertError(E);
				res.status(403).json({
					mensaje: "contraseña incorrecta",
				});
			}
		} else {
			let E = {
				nivel: "usuario",
				origen: "Servicio Usuarios",
				descripcion: "no existe usuario con ese correo",
				entrada: "Usuario " + user.Correo,
				mensaje: "no existe usuario con ese correo",
				stacktrace: "",
			};
			r2 = await usuarios.InsertError(E);
			res.status(403).json({
				mensaje: "no existe usuario con ese correo",
			});
		}
	} catch (e) {
		let E = {
			nivel: "Sistema",
			origen: "Servicio Usuarios",
			descripcion: "",
			entrada: "Usuario " + user.Correo,
			mensaje: e.message,
			stacktrace: e.stack,
		};
		r2 = await usuarios.InsertError(E);
		res.status(500).json({
			mensaje: e.message,
		});
	}
}
module.exports.login = login;

async function Clean(req, res, next) {
	try {
		getU = await usuarios.Clean();
		res.status(200).send("borrado");
	} catch (e) {
		console.log(e.menssage);
		res.status(500).json({
			mensaje: e.message,
		});
	}
}
module.exports.Clean = Clean;
