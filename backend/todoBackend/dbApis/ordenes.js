const pool = require("../config/database");

async function createOrden(orden) {
	let createSQL = "";
	let params = [];

	if (orden.idUsuario == 0) {
		createSQL =
			"INSERT INTO orden (id_usuario,id_admin, id_tipo_entrega, id_estado_orden, nombre, fecha, total, direccion, nit) VALUES ($1,$2, $3, 1, $4, $5, 0, $6, $7) RETURNING *";
		params = [
			orden.idUsuario,
			orden.id_admin,
			orden.idTipoEntrega,
			orden.nombre,
			orden.fecha,
			orden.direccion,
			orden.nit,
		];
	} else {
		createSQL =
			"INSERT INTO orden (id_usuario,id_admin, id_tipo_entrega, id_estado_orden, nombre, fecha, total, direccion, nit) VALUES ($1,$2, $3, 1, $4, $5, 0, $6, $7) RETURNING *";
		params = [
			orden.idUsuario,
			orden.id_admin,
			orden.idTipoEntrega,
			orden.nombre,
			orden.fecha,
			orden.direccion,
			orden.nit,
		];
	}

	try {
		var res = await pool.query(createSQL, params);

		if (res.rowCount == 1) {
			idNuevaOrden = res.rows[0].id_orden;
			console.log("Id nueva orden: " + idNuevaOrden);

			try {
				let suma = 0.0;
				let cont = 0;

				for (let producto of orden.productos) {
					const insertP =
						"INSERT INTO orden_producto (id_orden, id_producto, cantidad) VALUES ($1, $2, $3)";
					const par = [idNuevaOrden, producto.idProducto, producto.cantidad];

					await pool.query(insertP, par);

					suma += producto.precio * producto.cantidad;
					cont++;
				}
				console.log("Total: " + suma);

				try {
					const updateOrden = "UPDATE orden SET total = $1 WHERE id_orden = $2";
					const parU = [suma, idNuevaOrden];
					await pool.query(updateOrden, parU);

					console.log("Total actualizado");

					let name = await pool.query(
						"SELECT nombre FROM usuario WHERE id_usuario  = $1",
						[orden.idUsuario]
					);

					let Logg = {
						tipo: "Creacion",
						nombre: name.rows[0].nombre,
						usuario: orden.idUsuario,
						nivel: "Cliente",
						origen: "Servicio Usuarios",
						informacion: JSON.stringify({
							mensaje: "Orden creada y total obtenido",
							data: {
								idOrden: idNuevaOrden,
								total: suma,
								contadorProductos: cont,
							},
						}),
					};
					await InsertLog(Logg);

					return {
						tipo: "OK",
						estado: 201,
						message: "Orden creada satisfactoriamente",
						idOrden: idNuevaOrden,
					};
				} catch (error) {
					console.log(error);

					let E = {
						nivel: "Sistema",
						origen: "Servicio Ordenes",
						descripcion: "", //Solo si es error de usuario
						entrada: "", //Solo si es error de usuario
						mensaje: "Error al actualizar el total de la orden", //Solo si es error de sistema
						stacktrace: err.stack, //Solo si es error de sistema
					};
					await InsertError(E);

					return {
						tipo: "ERR",
						estado: 409,
						message: "No se pudo actualizar el total de la orden",
						stack: error.message,
					};
				}
			} catch (err) {
				console.log(err);

				let E = {
					nivel: "Sistema",
					origen: "Servicio Ordenes",
					descripcion: "", //Solo si es error de usuario
					entrada: "", //Solo si es error de usuario
					mensaje: "No se pudo ingresar un producto a la tabla orden_producto", //Solo si es error de sistema
					stacktrace: err.stack, //Solo si es error de sistema
				};
				await InsertError(E);

				return {
					tipo: "ERR",
					estado: 409,
					message:
						"No se pudo insertar algun producto. Orden creada No. " +
						idNuevaOrden,
					stack: err.message,
				};
			}
		} else {
			let E = {
				nivel: "Sistema",
				origen: "Servicio Ordenes",
				descripcion: "", //Solo si es error de usuario
				entrada: "", //Solo si es error de usuario
				mensaje: "No se pudo insertar la orden en la BD", //Solo si es error de sistema
				stacktrace: "", //Solo si es error de sistema
			};
			await InsertError(E);

			return {
				tipo: "ERR",
				estado: 409,
				message: "No se pudo insertar la orden",
				stack: "",
			};
		}
	} catch (e) {
		console.log(e);

		let E = {
			nivel: "Sistema",
			origen: "Servicio Ordenes",
			descripcion: "", //Solo si es error de usuario
			entrada: "", //Solo si es error de usuario
			mensaje: e.message, //Solo si es error de sistema
			stacktrace: e.stack, //Solo si es error de sistema
		};
		await InsertError(E);

		return {
			tipo: "ERR",
			estado: 404,
			message: "Error inesperado en servidor",
			stack: e.message,
		};
	}
}
module.exports.create = createOrden;

async function actualizarOrden(orden) {
	/*
        ESTADOS DE ORDEN
            1 - Nueva Orden
            2 - En Preparacion
            3 - En Camino
            4 - Entregada
            5 - Cancelada
            6 - Pagada
    */
	const upOrden = "UPDATE orden SET id_estado_orden = $1 WHERE id_orden = $2";
	const params = [orden.nuevoEstado, orden.idOrden];

	try {
		var res = await pool.query(upOrden, params);

		if (res.rowCount == 1) {
			let name = await pool.query(
				"SELECT nombre FROM usuario WHERE id_usuario  = $1",
				[orden.idUsuario]
			);
			let tipoE = await pool.query(
				"SELECT nombre FROM estado_orden WHERE id_estado_orden  = $1",
				[orden.nuevoEstado]
			);

			let Logg = {
				tipo: "Modificacion",
				nombre: name.rows[0].nombre,
				usuario: orden.idUsuario,
				nivel: orden.tipoUsuario == 1 ? "Administrador" : "Cliente",
				origen: "Servicio Usuarios",
				informacion: JSON.stringify({
					mensaje: "Orden actualizada",
					data: {
						idOrden: orden.idOrden,
						nuevoEstado: tipoE.rows[0].nombre,
					},
				}),
			};
			await InsertLog(Logg);

			return {
				tipo: "OK",
				estado: 201,
				message: "Orden actualizada satisfactoriamente",
				idOrden: orden.idOrden,
				estadoOrden: tipoE.rows[0].nombre,
			};
		} else {
			let E = {
				nivel: "Sistema",
				origen: "Servicio Ordenes",
				descripcion: "", //Solo si es error de usuario
				entrada: "", //Solo si es error de usuario
				mensaje: "No se pudo actualizar la orden en la BD", //Solo si es error de sistema
				stacktrace: "", //Solo si es error de sistema
			};
			await InsertError(E);

			return {
				tipo: "ERR",
				estado: 409,
				message: "No se pudo actualizar la orden",
				stack: "",
			};
		}
	} catch (e) {
		console.log(e);

		let E = {
			nivel: "Sistema",
			origen: "Servicio Ordenes",
			descripcion: "", //Solo si es error de usuario
			entrada: "", //Solo si es error de usuario
			mensaje: e.message, //Solo si es error de sistema
			stacktrace: e.stack, //Solo si es error de sistema
		};
		await InsertError(E);

		return {
			tipo: "ERR",
			estado: 404,
			message: "Error inesperado en servidor",
			stack: e.message,
		};
	}
}
module.exports.update = actualizarOrden;

//=======================================TRAER TODAS LAS ORDENES DE UN USUARIO
async function getOrdenes(idUsuario, idOrden, idAdmin) {
	try {
		var res = "";
		if (idAdmin != -1) {
			res = await pool.query("SELECT * FROM orden WHERE id_admin = " + idAdmin);
		} else if (idUsuario == -1) {
			res = await pool.query("SELECT * FROM orden;");
		} else if (idOrden > 0) {
			res = await pool.query(
				"SELECT * FROM orden  WHERE id_usuario = " +
					idUsuario +
					"and id_orden  =" +
					idOrden
			);
		} else {
			res = await pool.query(
				"SELECT * FROM orden  WHERE id_usuario = " + idUsuario
			);
		}

		if (res != false) {
			return res.rows;
		} else {
			let E = {
				nivel: "Sistema",
				origen: "Servicio Ordenes",
				descripcion: "", //Solo si es error de usuario
				entrada: "", //Solo si es error de usuario
				mensaje: e.message, //Solo si es error de sistema
				stacktrace: e.stack, //Solo si es error de sistema
			};
			await InsertError(E);

			return {
				tipo: "ERR",
				estado: 404,
				message: "Error inesperado en servidor",
				stack: e.message,
			};
		}
	} catch (e) {
		console.log(e);

		let E = {
			nivel: "Sistema",
			origen: "Servicio Ordenes",
			descripcion: "", //Solo si es error de usuario
			entrada: "", //Solo si es error de usuario
			mensaje: e.message, //Solo si es error de sistema
			stacktrace: e.stack, //Solo si es error de sistema
		};
		await InsertError(E);

		return {
			tipo: "ERR",
			estado: 404,
			message: "Error inesperado en servidor",
			stack: e.message,
		};
	}
}
module.exports.getOrdenes = getOrdenes;

// ============================================

async function InsertLog(Log) {
	var fecha = new Date();
	const insertL =
		"INSERT INTO  log (fecha, tipo_accion, nombre_usuario, id_usuario, nivel_accion, origen, informacion) VALUES ('" +
		fecha +
		"','" +
		Log.tipo +
		"','" +
		Log.nombre +
		"','" +
		Log.usuario +
		"','" +
		Log.nivel +
		"','" +
		Log.origen +
		"','" +
		Log.informacion +
		"')";

	try {
		await pool.query(insertL);
	} catch (e) {
		console.log(e.message); // 30

		let E = {
			nivel: "Sistema",
			origen: "Servicio Ordenes",
			descripcion: "", //Solo si es de usuario
			entrada: "", //Solo si es de usuario
			mensaje: e.message,
			stacktrace: e.stack,
		};
		await InsertError(E);

		return e.message;
	}
}
module.exports.InsertLog = InsertLog;

async function InsertError(Erro) {
	var fecha = new Date();
	const insertE =
		"INSERT INTO error (fecha, nivel_error, origen, descripcion, entrada, mensaje_error, stacktrace) VALUES ('" +
		fecha +
		"','" +
		Erro.nivel +
		"','" +
		Erro.origen +
		"','" +
		Erro.descripcion +
		"','" +
		Erro.entrada +
		"','" +
		Erro.mensaje +
		"','" +
		Erro.stacktrace +
		"')";

	try {
		await pool.query(insertE);
	} catch (e) {
		console.log("Error en Registro Error: " + e.message); // 30
		return e.message;
	}
}
module.exports.InsertError = InsertError;
