const pool = require("../config/database");

async function createUser(user) {
	console.log(user);
	const createSQL = `
    INSERT INTO usuario (tipo_usuario, nombre, apellido, correo, dpi, direccion, password)
      VALUES(
        ${user.tipo},
        '${user.nombre}',
        '${user.apellido}',
        '${user.correo}',
        '${user.dpi}',
        '${user.direccion}',
        '${user.password}'
        )
  `;

	try {
		var resultado = await pool.query(createSQL);

		if (resultado.rowCount > 0) {
			return true;
		}
	} catch (e) {
		console.log("aca error");
		console.log(e);
		return e.message;
	}
}
module.exports.create = createUser;

async function updateUser(user) {
	const updateSQL = `UPDATE usuario SET nombre = '${user.nombre}', apellido = '${user.apellido}', correo = '${user.correo}', dpi = '${user.dpi}', direccion = '${user.direccion}', password = '${user.password}' WHERE ID_Usuario = '${user.id}'`;

	try {
		var resultado = await pool.query(updateSQL);

		//console.log(resultado)

		if (resultado.rowCount > 0) {
			//console.log('Hay cambios en BD')
			return true;
		} else {
			//console.log('No hay cambios en BD')
			return false;
		}
	} catch (e) {
		console.log(e);

		return e.message;
	}
}
module.exports.update = updateUser;

async function getUser(user) {
	const getUserC = "SELECT * FROM USUARIO WHERE Correo = '" + user.Correo + "'";

	try {
		var resultado = await pool.query(getUserC);
		if (resultado != false) {
			if (resultado.rowCount > 0) {
				return resultado.rows[0];
			} else {
				return false;
			}
		}
	} catch (e) {
		console.log(e); // 30
		let E = {
			nivel: "Sistema",
			origen: "Servicio Usuarios",
			descripcion: "",
			entrada: "Usuario " + user.Correo,
			mensaje: e.message,
			stacktrace: e.stack,
		};
		r2 = await InsertError(E);

		return e.message;
	}
}
module.exports.getUser = getUser;

async function InsertError(Erro) {
	var fecha = new Date();
	const insertE =
		"INSERT INTO error (fecha, nivel_error, origen, descripcion, entrada, mensaje_error, stacktrace) VALUES ('" +
		fecha +
		"','" +
		Erro.nivel +
		"','" +
		Erro.origen +
		"','" +
		Erro.descripcion +
		"','" +
		Erro.entrada +
		"','" +
		Erro.mensaje +
		"','" +
		Erro.stacktrace +
		"')";

	try {
		await pool.query(insertE);
	} catch (e) {
		console.log(e.message); // 30
		return e.message;
	}
}
module.exports.InsertError = InsertError;

async function InsertLog(Log) {
	var fecha = new Date();
	const insertL =
		"INSERT INTO  log (fecha, tipo_accion, nombre_usuario, id_usuario, nivel_accion, origen, informacion) VALUES ('" +
		fecha +
		"','" +
		Log.tipo +
		"','" +
		Log.nombre +
		"','" +
		Log.usuario +
		"','" +
		Log.nivel +
		"','" +
		Log.origen +
		"','" +
		Log.informacion +
		"')";

	try {
		var resultado = await pool.query(insertL);
		return resultado;
	} catch (e) {
		console.log(e.message); // 30
		let E = {
			nivel: "Sistema",
			origen: "Servicio Usuarios",
			descripcion: "",
			entrada: "Usuario " + Log.nombre,
			mensaje: e.message,
			stacktrace: e.stack,
		};
		r2 = await InsertError(E);
		return e.message;
	}
}
module.exports.InsertLog = InsertLog;

async function DeletePrueba() {
	const deletev = "DELETE FROM usuario WHERE dpi = '0000000000'";

	try {
		var resultado = await pool.query(deletev);
		return resultado;
	} catch (e) {
		return e.message;
	}
}
module.exports.Clean = DeletePrueba;
