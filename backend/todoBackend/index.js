const express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const cors = require("cors");
const pool = require("./config/database");

const router = require("./router");
const app = express();

//settings
const port = process.env.PORT || 3001;
app.set("json spaces", 2);

app.use(morgan("dev"));
app.use(cors());
app.use(bodyParser.json());
app.use(
	bodyParser.urlencoded({
		extended: true,
	})
);

//Rutas
app.get("/", function (req, res) {
	res.send("Ordenes");
});
// obtener productos
app.get("/restaurante/:id", (req, res) => {
	const sql = `SELECT P.id_producto, P.nombre, P.precio, P.fotografia, P.oferta, P.id_admin, U.nombre AS restaurante
    FROM producto P, usuario U 
    WHERE P.id_admin = U.id_usuario
    AND P.id_admin = ${req.params.id};`;

	pool.query(sql, (err, data) => {
		if (err) {
			createErrorLog(
				"Sistema",
				"product/read",
				"No se pudo obtener los productos del restaurante",
				"",
				"error de la base de datos",
				err
			).finally(() =>
				res.status(500).json({ mensaje: "error de base de datos" })
			);
			return;
		}
		res.json({ productos: data.rows });
	});
});

app.get("/restaurante/info/get", (req, res) => {
	const sql =
		"SELECT U.nombre,U.apellido,  U.id_usuario FROM usuario U WHERE U.tipo_usuario = 1;";

	pool.query(sql, (err, data) => {
		if (err) {
			createErrorLog(
				"Sistema",
				"product/read",
				"No se pudo obtener los productos del restaurante",
				"",
				"error de la base de datos",
				err
			).finally(() => res.status(500).json({ mensaje: err.message }));
			return;
		}
		res.json({ restaurantes: data.rows });
	});
});

// productos crear
app.post("/producto/create", (req, res) => {
	const { nombre, precio, fotografia, oferta, id_admin } = req.body;

	if (nombre && parseFloat(precio) && fotografia) {
		console.log(oferta);
		pool.query(
			"INSERT INTO producto(nombre, precio, fotografia, oferta, id_admin) VALUES ($1, $2, $3, $4, $5);",
			[nombre, precio, fotografia, oferta, id_admin],
			(err, data) => {
				console.log(err);
				if (err) {
					createErrorLog(
						"Sistema",
						"product/create",
						"No se pudo crear el producto",
						JSON.stringify(req.body),
						"atributos no validos",
						err
					).finally(() =>
						res.status(400).json({ mensaje: "atributos no validos" })
					);
					return;
				}
				createLog(
					"Creacion",
					"Admin",
					"1",
					"Admin",
					"product/create",
					JSON.stringify(req.body)
				).finally(() =>
					res
						.status(201)
						.json({ nombre: nombre, precio: precio, fotografia: fotografia })
				);
			}
		);
	} else {
		createErrorLog(
			"Usuario",
			"product/create",
			"No se pudo crear el producto",
			JSON.stringify(req.body),
			"atributos no validos",
			""
		).finally(() => res.status(400).json({ mensaje: "atributos no validos" }));
	}
});

// productos eliminar
app.delete("/producto/:id", (req, res) => {
	if (parseInt(req.params.id)) {
		pool.query(
			"DELETE FROM producto WHERE id_producto = $1",
			[req.params.id],
			(err, data) => {
				if (err) {
					createErrorLog(
						"Sistema",
						"product/delete",
						"No se pudo eliminar el producto",
						req.params.id,
						"error de base de datos",
						err
					).finally(() =>
						res.status(500).json({ mensaje: "error de base de datos" })
					);
					return;
				}
				createLog(
					"Eliminacion",
					"Admin",
					"1",
					"Admin",
					"product/delete",
					req.params.id
				).finally(() =>
					res.status(200).json({ mensaje: "producto eliminado" })
				);
			}
		);
	} else {
		createErrorLog(
			"Usuario",
			"product/delete",
			"No se pudo eliminar el producto",
			req.params.id,
			"id no valido",
			""
		).finally(() => res.status(400).json({ mensaje: "id no valido" }));
	}
});

// productos actualizar
app.put("/producto/update", (req, res) => {
	const { id_producto, nombre, precio, fotografia, oferta } = req.body;
	if (
		parseInt(id_producto) &&
		nombre &&
		parseFloat(precio) &&
		fotografia &&
		parseFloat(oferta)
	) {
		pool.query(
			"UPDATE producto SET nombre = $1, precio = $2, fotografia = $3, oferta = $4 WHERE id_producto = $5;",
			[nombre, precio, fotografia, oferta, id_producto],
			(err, data) => {
				if (err) {
					createErrorLog(
						"Sistema",
						"product/update",
						"No se pudo actualizar el producto",
						JSON.stringify(req.body),
						"atributos no validos",
						err
					).finally(() =>
						res.status(400).json({ mensaje: "atributos no validos" })
					);
					return;
				}
				createLog(
					"Actualizacion",
					"Admin",
					"1",
					"Admin",
					"product/update",
					JSON.stringify(req.body)
				).finally(() =>
					res.status(200).json({
						id_producto: id_producto,
						nombre: nombre,
						precio: precio,
						fotografia: fotografia,
						oferta: oferta,
					})
				);
			}
		);
	} else {
		createErrorLog(
			"Sistema",
			"product/update",
			"No se pudo actualizar el producto",
			JSON.stringify(req.body),
			"atributos no validos",
			""
		).finally(() => res.status(400).json({ mensaje: "atributos no validos" }));
	}
});

// productos leer
app.get("/producto/read", (req, res) => {
	const sql = `SELECT P.id_producto, P.nombre, P.precio, P.fotografia, P.oferta, P.id_admin, U.nombre AS restaurante
    FROM producto P, usuario U 
    WHERE P.id_admin = U.id_usuario;`;

	pool.query(sql, (err, data) => {
		if (err) {
			createErrorLog(
				"Sistema",
				"product/read",
				"No se pudo obtener los productos",
				"",
				"error de la base de datos",
				err
			).finally(() =>
				res.status(500).json({ mensaje: "error de base de datos" })
			);
			return;
		}
		res.json({ productos: data.rows });
	});
});

app.get("/producto/:id", (req, res) => {
	const sql = `SELECT P.id_producto, P.nombre, P.precio, P.fotografia, P.oferta, P.id_admin, U.nombre AS restaurante
    FROM producto P, usuario U 
    WHERE P.id_admin = U.id_usuario
    AND P.id_producto = ${req.params.id};`;

	pool.query(sql, (err, data) => {
		if (err) {
			createErrorLog(
				"Sistema",
				"product/read",
				"No se pudo obtener un producto",
				req.params.id,
				"error de la base de datos",
				err
			).finally(() =>
				res.status(500).json({ mensaje: "error de base de datos" })
			);
			return;
		}
		res.json({ producto: data.rows[0] });
	});
});


app.use("/", router);

function createLog(actionType, username, user_id, actionLevel, source, info) {
	return new Promise((resolve, reject) => {
		pool.query(
			`INSERT INTO Log(fecha, tipo_accion, nombre_usuario, id_usuario, nivel_accion, origen, informacion)
             VALUES ($1, $2, $3, $4, $5, $6, $7)`,
			[Date(), actionType, username, user_id, actionLevel, source, info],
			(err, data) => {
				if (err) {
					reject(err);
				} else {
					resolve(data);
				}
			}
		);
	});
}

function createErrorLog(
	errorLevel,
	source,
	info,
	entry,
	errorMessage,
	stacktrace
) {
	return new Promise((resolve, reject) => {
		pool.query(
			`INSERT INTO Error(fecha, nivel_error, origen, descripcion, entrada, mensaje_error, stacktrace)
        VALUES ($1, $2, $3, $4, $5, $6, $7)`,
			[Date(), errorLevel, source, info, entry, errorMessage, stacktrace],
			(err, data) => {
				if (err) {
					reject(err);
				} else {
					resolve(data);
				}
			}
		);
	});
}

app.listen(port, (err) => {
	if (err) console.log("Ocurrio un error"), process.exit(1);

	console.log(`Escuchando en el puerto ${port}`);
	//console.log(process.env.PORT);
});
