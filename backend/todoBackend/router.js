const express = require("express");
const router = new express.Router();

const ordenes = require("./controllers/ordenes");
const usuarios = require("./controllers/usuarios");

router
	.route("/usuarios/:id?")
	.get(usuarios.get)
	.post(usuarios.post)
	.put(usuarios.put);

router.route("/login").post(usuarios.login);
router.route("/Clean").get(usuarios.Clean);

// ordenes
router
	.route("/ordenes/:id?")
	.post(ordenes.post)
	.put(ordenes.put)
	.get(ordenes.getOrdenes);

router.route("/ordenes/:id/:idOrden").get(ordenes.getOrden);

router.route("/all/Ordenes/:idAdmin").get(ordenes.get);

module.exports = router;
 