CREATE TABLE IF NOT EXISTS Tipo_Usuario(
    ID_Tipo_Usuario SERIAL,
    Nombre TEXT NOT NULL,
    PRIMARY KEY(ID_Tipo_Usuario),
    UNIQUE(Nombre)
);

CREATE TABLE IF NOT EXISTS Usuario(
    ID_Usuario SERIAL,
    Tipo_Usuario INT NOT NULL,
    Nombre TEXT NOT NULL,
    Apellido TEXT NOT NULL,
    Correo TEXT NOT NULL,
    DPI TEXT NOT NULL,
    Direccion TEXT NOT NULL,
    Password TEXT NOT NULL,
    PRIMARY KEY(ID_Usuario),
    FOREIGN KEY(Tipo_Usuario) REFERENCES Tipo_Usuario(ID_Tipo_Usuario),
    UNIQUE(Correo),
    UNIQUE(DPI),
    UNIQUE(DPI)
);

CREATE TABLE IF NOT EXISTS Producto(
    ID_Producto SERIAL,
    Nombre TEXT NOT NULL,
    Precio DECIMAL NOT NULL,
    Fotografia TEXT NOT NULL,
    Oferta DECIMAL NOT NULL DEFAULT 0.0,
    ID_Admin INT NOT NULL,
    PRIMARY KEY(ID_Producto),
    FOREIGN KEY(ID_Admin) REFERENCES Usuario(ID_Usuario)
);

CREATE TABLE IF NOT EXISTS Carrito(
    ID_Usuario INT NOT NULL,
    ID_Producto INT NOT NULL,
    Cantidad INT NOT NULL,
    FOREIGN KEY(ID_Usuario) REFERENCES Usuario(ID_Usuario),
    FOREIGN KEY(ID_Producto) REFERENCES Producto(ID_Producto) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS Estado_Orden(
    ID_Estado_Orden SERIAL,
    Nombre TEXT NOT NULL,
    PRIMARY KEY (ID_Estado_Orden),
    UNIQUE(Nombre)
);

CREATE TABLE IF NOT EXISTS Tipo_Entrega(
    ID_Tipo_Entrega SERIAL,
    Nombre TEXT NOT NULL,
    PRIMARY KEY(ID_Tipo_Entrega),
    UNIQUE(Nombre)
);

CREATE TABLE IF NOT EXISTS Orden(
    ID_Orden SERIAL,
    ID_Usuario INT,
    ID_Admin INT NOT NULL,
    ID_Tipo_Entrega INT NOT NULL,
    ID_Estado_Orden INT NOT NULL,
    Nombre TEXT NOT NULL,
    Fecha DATE NOT NULL,
    Total DECIMAL NOT NULL,
    Direccion TEXT NOT NULL,
    NIT TEXT NOT NULL,
    PRIMARY KEY(ID_Orden),
    FOREIGN KEY(ID_Usuario) REFERENCES Usuario(ID_Usuario),
    FOREIGN KEY(ID_Admin) REFERENCES Usuario(ID_Usuario),
    FOREIGN KEY(ID_Tipo_Entrega) REFERENCES Tipo_Entrega(ID_Tipo_Entrega),
    FOREIGN KEY(ID_Estado_Orden) REFERENCES Estado_Orden(ID_Estado_Orden)
);

CREATE TABLE IF NOT EXISTS Orden_Producto(
    ID_Orden INT NOT NULL,
    ID_Producto INT NOT NULL,
    Cantidad INT NOT NULL,
    FOREIGN KEY (ID_Orden) REFERENCES Orden(ID_Orden),
    FOREIGN KEY (ID_Producto) REFERENCES Producto(ID_Producto) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS Log(
    ID_Log SERIAL,
    Fecha TEXT,
    Tipo_Accion TEXT,
    Nombre_Usuario TEXT,
    ID_Usuario TEXT,
    Nivel_Accion TEXT,
    Origen TEXT,
    Informacion TEXT,
    PRIMARY KEY(ID_Log)
);

CREATE TABLE IF NOT EXISTS Error(
    ID_Error SERIAL,
    Fecha TEXT,
    Nivel_Error TEXT,
    Origen TEXT,
    Descripcion TEXT,
    Entrada TEXT,
    Mensaje_Error TEXT,
    Stacktrace TEXT,
    PRIMARY KEY(ID_Error)
);

INSERT INTO  Tipo_Usuario(Nombre)
VALUES('Administrador');
INSERT INTO  Tipo_Usuario(Nombre)
VALUES('Cliente');

INSERT INTO  Estado_Orden(Nombre)
VALUES('Nueva Orden');
INSERT INTO  Estado_Orden(Nombre)
VALUES('En Preparación');
INSERT INTO  Estado_Orden(Nombre)
VALUES('En Camino');
INSERT INTO  Estado_Orden(Nombre)
VALUES('Entregada');
INSERT INTO  Estado_Orden(Nombre)
VALUES('Cancelada');
INSERT INTO  Estado_Orden(Nombre)
VALUES('Pagada');

INSERT INTO  Tipo_Entrega(Nombre)
VALUES('En Tienda');
INSERT INTO  Tipo_Entrega(Nombre)
VALUES('A Domicilio');

INSERT INTO Usuario(ID_Usuario, Tipo_Usuario, Nombre, Apellido, Correo, DPI, Direccion, Password)
VALUES(0, 2, '', '', '', '', '','');

INSERT INTO Usuario(Tipo_Usuario, Nombre, Apellido, Correo, DPI, Direccion, Password)
VALUES(1, 'admin', 'admin', 'admin@fd.com', 'admin', 'admin','hgK3i8hbUeWiXpgns4wXHg=='); /*contraseña 12345678*/
/*
UPDATE USUARIO
SET tipo_usuario = 1
WHERE id_usuario = 1;
*/