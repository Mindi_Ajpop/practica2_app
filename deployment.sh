#!/bin/bash
sudo docker-compose down
sudo docker build --pull --no-cache --tag marckomatic/backend --file ./backend/todoBackend/Dockerfile .
sudo docker push image marckomatic/backend
npm run build --prefix ./frontend
sudo docker build --pull --no-cache --tag marckomatic/web --file ./frontend/Dockerfile .
sudo docker push image marckomatic/web

#yes | docker image prune
sudo docker-compose -f docker-compose.yml up -d --build
