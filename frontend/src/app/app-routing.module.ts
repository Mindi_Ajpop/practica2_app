import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { HomeClientComponent } from './components/home-client/home-client.component';
import { HomeAdminComponent } from './components/home-admin/home-admin.component';
import { UpdateInfoComponent } from './components/update-info/update-info.component';
import { ProductosComponent } from './components/productos/productos.component';
import { EditProductComponent } from './components/edit-product/edit-product.component';
import { RegisterComponent } from './components/register/register.component';
import { CreateProductComponent } from './components/create-product/create-product.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'Home/Admin', component: HomeAdminComponent },
  { path: 'Home/Cliente', component: HomeClientComponent },
  { path: 'updateMyInfo', component: UpdateInfoComponent },
  { path: 'productos', component: ProductosComponent },
  { path: 'updateProduct', component: EditProductComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'createProduct', component: CreateProductComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
