import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UpdateServiceService } from '../app/services/update-service.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'frontend';

  sesion: boolean;
  tipo = '0';

  constructor(
    private router: Router,
    public auth: UpdateServiceService,
    public cookieService: CookieService
  ) {}

  ngOnInit(): void {
    this.router.events.subscribe((event) => {
      if (event.constructor.name === 'NavigationEnd') {
        this.sesion = this.auth.isLoggedIn;
      }
    });
    this.tipo = this.cookieService.get('Tipo');
  }

  defHome() {
    this.tipo = this.cookieService.get('Tipo');
    if (this.tipo == '1') {
      this.router.navigateByUrl('Home/Admin');
    } else if (this.tipo == '2') {
      this.router.navigateByUrl('Home/Cliente');
    } else {
      this.router.navigateByUrl('/');
    }
  }

  logout() {
    //borrar cookies
    this.cookieService.deleteAll();
    console.log('Cerrando Sesión');
    this.router.navigateByUrl('/').then(() => window.location.reload());
  }
}
