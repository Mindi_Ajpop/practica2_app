import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { UpdateServiceService } from './services/update-service.service';
import { CookieService } from 'ngx-cookie-service';
import { HomeAdminComponent } from './components/home-admin/home-admin.component';
import { HomeClientComponent } from './components/home-client/home-client.component';
import { UpdateInfoComponent } from './components/update-info/update-info.component';
import { ProductosComponent } from './components/productos/productos.component';
import { EditProductComponent } from './components/edit-product/edit-product.component';
import { RegisterComponent } from './components/register/register.component';
import { CommonModule } from '@angular/common';
import { CreateProductComponent } from './components/create-product/create-product.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    HomeAdminComponent,
    HomeClientComponent,
    UpdateInfoComponent,
    ProductosComponent,
    EditProductComponent,
    RegisterComponent,
    CreateProductComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CommonModule,
  ],
  providers: [CookieService, UpdateServiceService],
  bootstrap: [AppComponent],
})
export class AppModule {}
