import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { FormBuilder, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.scss'],
})
export class CreateProductComponent implements OnInit {
  createProductForm = this.fb.group({
    productName: ['', Validators.required],
    productPrice: ['', Validators.required],
    productImage: ['', Validators.required],
    offerPrice: ['', Validators.required],
  });

  productImageData = '';
  successAlert = false;
  errorAlert = false;

  constructor(
    private fb: FormBuilder,
    private httpClient: HttpClient,
    private cookieService: CookieService
  ) {}

  ngOnInit(): void {}

  onSubmit(): void {
    const {
      productName,
      productPrice,
      offerPrice,
    } = this.createProductForm.value;

    this.httpClient
      .post(`http://${environment.api}:3001/producto/create/`, {
        nombre: productName,
        precio: productPrice,
        fotografia: this.productImageData,
        oferta: offerPrice,
        id_admin: this.cookieService.get('ID'),
      })
      .subscribe(
        (data) => {
          this.createProductForm.reset();
          this.productImageData = '';
          this.successAlert = true;
          setTimeout(() => {
            this.successAlert = false;
          }, 3000);
        },
        (error) => {
          console.error(error);
          this.errorAlert = true;
          setTimeout(() => {
            this.errorAlert = false;
          }, 3000);
        }
      );
  }

  onFileChange(event: any): void {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.productImageData = reader.result.toString();
      };
    }
  }
}
