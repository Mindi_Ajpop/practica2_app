import { Component, OnInit } from '@angular/core';
import { EditProductService } from '../../services/edit-product.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss'],
})
export class EditProductComponent implements OnInit {
  createProductForm = this.fb.group({
    productName: ['', Validators.required],
    productPrice: ['', Validators.required],
    productImage: [''],
    offerPrice: ['', Validators.required],
  });

  productImageData = '';
  successAlert = false;
  errorAlert = false;

  idProducto: number;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private editService: EditProductService,
    private httpClient: HttpClient
  ) {}

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.idProducto = params['idProducto'];
      this.editService.getProduct(this.idProducto).subscribe(
        (result) => {
          if (result.producto) {
            this.productImageData = result.producto['fotografia'];
            this.createProductForm.patchValue({
              productName: result.producto['nombre'],
              productPrice: result.producto['precio'],
              offerPrice: result.producto['oferta'],
            });
          } else {
            this.router.navigateByUrl('/productos');
          }
        },
        (error) => {
          this.router.navigateByUrl('/productos');
        }
      );
    });
  }

  onSubmit(): void {
    const {
      productName,
      productPrice,
      offerPrice,
    } = this.createProductForm.value;
    this.httpClient
      .put(`http://${environment.api}:8002/`, {
        id_producto: this.idProducto,
        nombre: productName,
        precio: productPrice,
        fotografia: this.productImageData,
        oferta: offerPrice,
      })
      .subscribe(
        (data) => {
          this.successAlert = true;
          setTimeout(() => {
            this.successAlert = false;
          }, 3000);
        },
        (error) => {
          console.error(error);
          this.errorAlert = true;
          setTimeout(() => {
            this.errorAlert = false;
          }, 3000);
        }
      );
  }

  onFileChange(event: any): void {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.productImageData = reader.result.toString();
      };
    }
  }
}
