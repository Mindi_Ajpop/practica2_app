import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { OrdenesService } from '../../services/ordenes.service';

@Component({
  selector: 'app-home-admin',
  templateUrl: './home-admin.component.html',
  styleUrls: ['./home-admin.component.scss'],
})
export class HomeAdminComponent implements OnInit {
  constructor(
    private router: Router,
    private ordenesService: OrdenesService,
    private cookieService: CookieService
  ) {}

  ngOnInit(): void {
    this.getOrdenes();
  }

  ordenes: any[];
  getOrdenes() {
    this.ordenesService.getAllOrdenes(this.cookieService.get('ID')).subscribe(
      (result) => {
        this.ordenes = result;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  crearProducto() {
    this.router.navigateByUrl('createProduct');
  }

  actualizar(orden: any) {
    const body = {
      id: orden.id_usuario,
      tipoUsuario: '2',
      idOrden: orden.id_orden,
      nuevoEstado: orden.id_estado_orden,
    };
    this.ordenesService.updateOrden(body).subscribe(
      (result) => {
        console.log(result);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  eliminar() {}
}
