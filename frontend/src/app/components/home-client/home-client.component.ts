import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { OrdenesService } from '../../services/ordenes.service';

@Component({
  selector: 'app-home-client',
  templateUrl: './home-client.component.html',
  styleUrls: ['./home-client.component.scss'],
})
export class HomeClientComponent implements OnInit {
  ordenes: any[];
  test: string;

  constructor(
    private ordenesService: OrdenesService,
    private cookieService: CookieService
  ) {}

  ngOnInit(): void {
    this.getOrdenes();
  }

  getOrdenes() {
    this.ordenesService.getOrdenes(this.cookieService.get('ID')).subscribe(
      (result) => {
        this.ordenes = result;
        console.log(this.ordenes);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  cancelar(orden: any) {
    const body = {
      id: orden.id_usuario,
      tipoUsuario: '1',
      idOrden: orden.id_orden,
      nuevoEstado: orden.id_estado_orden,
    };
    this.ordenesService.updateOrden(body).subscribe(
      (result) => {
        console.log(result);
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
