import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { UpdateServiceService } from 'src/app/services/update-service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  constructor(
    private router: Router,
    private cookieService: CookieService,
    private loginService: UpdateServiceService
  ) {}

  ngOnInit(): void {
    if (this.cookieService.getAll().AccessToken) {
      this.loginService.isLoggedIn = true;
      this.Redireccionar();
    }
  }

  verProductos(): void {
    this.router.navigateByUrl('/productos');
  }
  irRegistro(): void {
    this.router.navigateByUrl('/register');
  }
  irLogin(): void {
    this.router.navigateByUrl('/login');
  }

  Redireccionar(): void {
    // esto para acceder a los datos del usuario logueado
    console.log('cookie');
    console.log(this.cookieService.get('AccessToken'));
    console.log(this.cookieService.get('ID'));
    console.log(this.cookieService.get('Tipo'));

    var tipo = parseInt(this.cookieService.get('Tipo'));

    if (tipo == 1) {
      this.router.navigateByUrl('Home/Admin');
    } else if (tipo == 2) {
      this.router.navigateByUrl('Home/Cliente');
    }
  }
}
