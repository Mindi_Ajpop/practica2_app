import { Component, OnInit, ViewChild } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { UpdateServiceService } from '../../services/update-service.service';
import { NgbAlert } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import CryptoJS from 'crypto-js';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  ver: boolean;
  type: string;
  incorrecta: boolean;
  UserIncorrecto: boolean;
  alert: boolean;
  fallo: boolean;
  mensaje: string;

  @ViewChild('selfClosingAlert', { static: false }) selfClosingAlert: NgbAlert;

  Formulario: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private loginService: UpdateServiceService,
    private cookieService: CookieService,
    private router: Router
  ) {
    this.Formulario = this.formBuilder.group({
      User: ['', Validators.required],
      Password: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.type = 'password';
    this.ver = false;
    this.incorrecta = false;
    this.UserIncorrecto = false;
    this.alert = false;
  }

  Login(): void {
    if (this.Formulario.valid) {
      const key: string = 'abcdefg';
      const keyHex = CryptoJS.enc.Utf8.parse(key);

      var user = this.Formulario.value.User;
      var pass = CryptoJS.DES.encrypt(this.Formulario.value.Password, keyHex, {
        mode: CryptoJS.mode.ECB,
        padding: CryptoJS.pad.Pkcs7,
      }).toString();

      this.loginService.UserLogin(user, pass).subscribe(
        (result) => {
          console.log(result);
          if (result.status == 200) {
            this.cookieService.delete('AccessToken');
            this.cookieService.delete('ID');
            this.cookieService.delete('Tipo');
            this.cookieService.delete('Nombre');
            this.cookieService.delete('Direccion');
            this.loginService.getInfo(result.id).subscribe(
              (result) => {
                var dir: string = result[0].direccion;
                var dirTemp = dir.replace(' ', '_');
                this.cookieService.set('Nombre', result[0].nombre);
                this.cookieService.set('Direccion', dirTemp);
              },
              (error) => {
                console.log(error);
              }
            );
            this.cookieService.set('AccessToken', result['token']);
            this.cookieService.set('ID', result['id']);
            this.cookieService.set('Tipo', result['tipo']);
            this.loginService.isLoggedIn = true;

            this.alert = false;
            this.Redireccionar();
          }
        },
        (error) => {
          this.mensaje = error.error['mensaje'];
          console.log(error.error['mensaje']);
          this.alert = true;
          setTimeout(() => this.selfClosingAlert.close(), 5000);
        }
      );
    } else {
      console.log('ERROR');
    }
  }

  CambiarEstado(): any {
    if (this.ver) {
      this.ver = false;
      this.type = 'password';
    } else {
      this.ver = true;
      this.type = 'text';
    }
  }

  Redireccionar(): void {
    // esto para acceder a los datos del usuario logueado
    console.log('cookie');
    console.log(this.cookieService.get('AccessToken'));
    console.log(this.cookieService.get('ID'));
    console.log(this.cookieService.get('Tipo'));
    var tipo = parseInt(this.cookieService.get('Tipo'));
    if (tipo == 1) {
      this.router.navigateByUrl('Home/Admin');
    } else if (tipo == 2) {
      this.router.navigateByUrl('Home/Cliente');
    } else {
      this.alert = true;
      this.mensaje = 'ERROR DE SISTEMA INTENTELO DE NUEVO';
    }
  }
}
