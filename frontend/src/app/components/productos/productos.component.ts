import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { EditProductService } from '../../services/edit-product.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { OrdenesService } from '../../services/ordenes.service';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.scss'],
})
export class ProductosComponent implements OnInit {
  //VARIABLES EN LA VISTA DE LOS PRODUCTOS
  closeResult = '';
  nombre: string;
  precio: number;

  cantidad: number;
  direccion: string;
  tipoEntrega: number;
  total: number;
  model = 1;
  nombreOrden: string;
  nit: string;
  fecha;
  productos: any[];
  productosOferta: any[];
  tipoUser: string;
  productosCarrito: any[];
  restaurantes: any[];
  totalProductos: number;
  nombreRestaurante: string;
  numRestaurante: number;
  idAdminOrden: number;

  constructor(
    private router: Router,
    private productService: EditProductService,
    private cookieService: CookieService,
    private modalService: NgbModal,
    private ordenesService: OrdenesService
  ) {
    this.productos = [];
    this.productosOferta = [];
    this.productosCarrito = [];
    this.restaurantes = [];
    this.cantidad = 0;
    this.direccion = 'Tienda';
    this.total = 0;
    this.numRestaurante = 0;
    this.idAdminOrden = 0;
    this.nombreOrden = this.cookieService.get('Nombre');
    this.direccion = this.cookieService.get('Direccion').replace('_', ' ');
  }

  //METODO PARA ABRIR LOS MODAL
  open(content) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  //METODO AL CERRAR EL MODAL
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  //EJECUTAR AL INICIAR
  ngOnInit(): void {
    this.getRestaurantes();
    this.totalProductos = 0;
    //this.getProductos()
    this.tipoUser = this.cookieService.get('Tipo');
  }

  getRestaurantes() {
    this.productService.getRestaurantes().subscribe(
      (result) => {
        this.restaurantes = result.restaurantes;
        this.idAdminOrden = this.restaurantes[this.numRestaurante].id_usuario;
        this.getProductos(this.idAdminOrden);
        this.nombreRestaurante =
          this.restaurantes[this.numRestaurante].nombre +
          ' ' +
          this.restaurantes[this.numRestaurante].apellido;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  verAnterior() {
    if (this.numRestaurante > 0) {
      this.productos.length = 0;
      this.productosOferta.length = 0;
      this.numRestaurante--;
      this.getProductos(this.restaurantes[this.numRestaurante].id_usuario);
      this.idAdminOrden = this.restaurantes[this.numRestaurante].id_usuario;
      this.nombreRestaurante =
        this.restaurantes[this.numRestaurante].nombre +
        ' ' +
        this.restaurantes[this.numRestaurante].apellido;
    } else {
      this.nombreRestaurante = '';
    }
  }

  verSiguiente() {
    if (this.numRestaurante < this.restaurantes.length) {
      this.productos.length = 0;
      this.productosOferta.length = 0;
      this.numRestaurante++;
      this.getProductos(this.restaurantes[this.numRestaurante].id_usuario);
      this.idAdminOrden = this.restaurantes[this.numRestaurante].id_usuario;
      this.nombreRestaurante =
        this.restaurantes[this.numRestaurante].nombre +
        ' ' +
        this.restaurantes[this.numRestaurante].apellido;
    } else {
      this.nombreRestaurante = '';
    }
  }

  //METODO PARA OBTENER LOS PRODUCTOS DEL BACKEND
  getProductos(id) {
    this.productService.getProductos().subscribe(
      (result) => {
        for (let i = 0; i < result.productos.length; i++) {
          if (result.productos[i].id_admin == id) {
            if (result.productos[i].oferta == 0) {
              this.productos.push(result.productos[i]);
            } else {
              result.productos[i].oferta =
                result.productos[i].precio -
                (result.productos[i].oferta * result.productos[i].precio) / 100;
              this.productosOferta.push(result.productos[i]);
            }
          }
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  //METODO PARA AGREGAR PRODUCTOS AL CARRITO
  comprar(id, precio, nombre) {
    let buy = {
      idProducto: id,
      precio: precio,
      nombre: nombre,
      cantidad: 1,
      indice: this.totalProductos,
    };
    if (this.cantidad > 0) {
      for (let index = 0; index < this.cantidad; index++) {
        this.productosCarrito.push(buy);
      }
      this.totalProductos++;
      this.total += this.cantidad * precio;
    }
    this.nombreOrden = this.cookieService.get('Nombre');
    this.direccion = this.cookieService.get('Direccion').replace('_', ' ');
    console.log(this.productosCarrito);
    this.cantidad = 0;
  }

  //METODO PARA ELIMINAR UN PRODUCTO
  eliminar(id) {
    this.productService.deleteProduct(id).subscribe(
      (result) => {
        console.log(result);
        this.getProductos(id);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  //METODO PARA ELIMINAR UN PRODUCTO DEL CARRITO
  quitarProducto(num) {
    this.productosCarrito.splice(num, 1);
  }

  //METODO PARA REALIZAR LA ORDEN
  ordenar() {
    let fechaEntrega =
      this.fecha.year + '-' + this.fecha.month + '-' + this.fecha.day;
    let orden = {
      idTipoEntrega: this.model,
      nombre: this.nombreOrden,
      fecha: fechaEntrega,
      direccion: this.direccion,
      nit: this.nit,
      productos: this.productosCarrito,
      id_admin: this.idAdminOrden,
    };
    let iduser = '';
    console.log(this.cookieService.get('ID'));
    if (this.cookieService.get('ID') == '0') {
      iduser = '0';
    } else {
      iduser = this.cookieService.get('ID');
    }
    this.ordenesService.newOrden(orden, iduser).subscribe(
      (result) => {
        console.log(result);
        this.nombreOrden = '';
        this.nit = '';
        this.fecha = {};
        this.productosCarrito.length = 0;
        this.totalProductos = 0;
      },
      (error) => {
        console.log(error);
      }
    );
    this.total = 0;
  }

  //METODO PARA EDITAR UN PRODUCTO
  editar(id: any) {
    this.router.navigate(['/updateProduct'], {
      queryParams: { idProducto: id },
    });
  }
}
