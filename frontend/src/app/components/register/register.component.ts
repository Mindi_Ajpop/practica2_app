import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { RegistroUsuarioService } from '../../services/registro-usuario.service';
import CryptoJS from 'crypto-js';
import { Router } from '@angular/router';
import { UpdateServiceService } from 'src/app/services/update-service.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  alerta = 'success';
  mensaje = '';
  public usuario = {
    nombre: '',
    apellido: '',
    correo: '',
    dpi: '',
    direccion: '',
    area: '',
    telefono: '',
    password: '',
    confirm: '',
    tipo: '',
  };

  constructor(
    private registroServicio: RegistroUsuarioService,
    private router: Router,
    private loginService: UpdateServiceService,
    private cookieService: CookieService
  ) {}

  ngOnInit(): void {}

  registrar() {
    const key: string = 'abcdefg';
    const keyHex = CryptoJS.enc.Utf8.parse(key);
    if (
      this.usuario.nombre &&
      this.usuario.apellido &&
      this.usuario.correo &&
      this.usuario.dpi &&
      this.usuario.direccion &&
      this.usuario.area &&
      this.usuario.telefono &&
      this.usuario.password &&
      this.usuario.confirm
    ) {
      if (this.usuario.password === this.usuario.confirm) {
        this.usuario.password = CryptoJS.DES.encrypt(
          this.usuario.password,
          keyHex,
          {
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7,
          }
        ).toString();
        this.usuario.confirm = CryptoJS.DES.encrypt(
          this.usuario.confirm,
          keyHex,
          {
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7,
          }
        ).toString();
        this.registroServicio.registrarUsuario(this.usuario).subscribe(
          (result) => {
            this.mensaje = JSON.parse(JSON.stringify(result)).mensaje;
            this.alerta = 'success';
            //this.router.navigateByUrl('updateMyInfo');
            this.loginService
              .UserLogin(this.usuario.correo, this.usuario.password)
              .subscribe(
                (result) => {
                  console.log(result);
                  if (result.status == 200) {
                    this.cookieService.delete('AccessToken');
                    this.cookieService.delete('ID');
                    this.cookieService.delete('Tipo');
                    this.cookieService.delete('Nombre');
                    this.cookieService.delete('Direccion');
                    this.loginService.getInfo(result.id).subscribe(
                      (result) => {
                        var dir: string = result[0].direccion;
                        var dirTemp = dir.replace(' ', '_');
                        this.cookieService.set('Nombre', result[0].nombre);
                        this.cookieService.set('Direccion', dirTemp);
                      },
                      (error) => {
                        console.log(error);
                      }
                    );
                    this.cookieService.set('AccessToken', result['token']);
                    this.cookieService.set('ID', result['id']);
                    this.cookieService.set('Tipo', result['tipo']);
                    this.loginService.isLoggedIn = true;
                    this.Redireccionar();
                  }
                },
                (error) => {
                  this.mensaje = error.error['mensaje'];
                  console.log(error.error['mensaje']);
                }
              );
          },
          (error) => {
            this.alerta = 'danger';
            this.mensaje = 'DPI y/o Correo ya existe(n)';
          }
        );
      } else {
        this.alerta = 'danger';
        this.mensaje = 'Las contraseñas no coinciden';
      }
    } else {
      this.alerta = 'danger';
      this.mensaje = 'Llenar todos los datos';
    }
  }
  Redireccionar(): void {
    // esto para acceder a los datos del usuario logueado
    console.log('cookie');
    console.log(this.cookieService.get('AccessToken'));
    console.log(this.cookieService.get('ID'));
    console.log(this.cookieService.get('Tipo'));

    var tipo = parseInt(this.cookieService.get('Tipo'));

    if (tipo == 1) {
      this.router.navigateByUrl('Home/Admin');
    } else if (tipo == 2) {
      this.router.navigateByUrl('Home/Cliente');
    }
  }
}
