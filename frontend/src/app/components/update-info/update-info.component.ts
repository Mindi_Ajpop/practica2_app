import { Component, OnInit } from '@angular/core';
import { UpdateServiceService } from '../../services/update-service.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

import CryptoJS from 'crypto-js';
@Component({
  selector: 'app-update-info',
  templateUrl: './update-info.component.html',
  styleUrls: ['./update-info.component.scss'],
})
export class UpdateInfoComponent implements OnInit {
  alerta = 0;
  id: string;
  tipo: string;
  constructor(
    private UpdateService: UpdateServiceService,
    private cookieService: CookieService,
    private router: Router
  ) {
    this.reset();
  }

  //Obtener datos del usuario para usar de placeholder
  ngOnInit(): void {
    this.id = this.cookieService.get('ID');
    this.tipo = this.cookieService.get('Tipo');
    this.getInfoUser();
  }

  //Variables del usuario
  nombre: string;
  apellido: string;
  direccion: string;
  dpi: number;
  email: string;
  password: string;
  confirma: string;

  //Metodo para obtener los valores de los inputs -> validar las contrasenias -> encriptar -> enviar al servidor -> notificar el cambio
  onSubmit() {
    if (this.validatePasswords()) {
      this.id = this.cookieService.get('ID');
      this.tipo = this.cookieService.get('Tipo');

      //encripción nueva contraseña
      const key: string = 'abcdefg';
      const keyHex = CryptoJS.enc.Utf8.parse(key);

      var encript = CryptoJS.DES.encrypt(this.password, keyHex, {
        mode: CryptoJS.mode.ECB,
        padding: CryptoJS.pad.Pkcs7,
      }).toString();

      this.UpdateService.Update(
        this.id,
        this.nombre,
        this.apellido,
        this.email,
        this.dpi,
        this.direccion,
        encript,
        this.tipo
      ).subscribe(
        (result) => {
          this.router.navigateByUrl('Home/Cliente');
        },
        (error) => {
          this.nombre = '';
          this.apellido = '';
          this.dpi = 0;
          this.email = '';
          this.password = '';
          this.confirma = '';
          this.getInfoUser();
          console.log(error);
        }
      );
      this.alerta = 1;
    } else {
      console.log('Error las contraseñas no son iguales!!');
      this.nombre = '';
      this.apellido = '';
      this.dpi = 0;
      this.email = '';
      this.password = '';
      this.confirma = '';
      this.getInfoUser();
      this.alerta = 2;
    }
  }

  getInfoUser() {
    try {
      this.UpdateService.getInfo(this.id).subscribe(
        (result) => {
          this.nombre = result[0].nombre;
          this.apellido = result[0].apellido;
          this.direccion = result[0].direccion;
          this.dpi = result[0].dpi;
          this.email = result[0].correo;
        },
        (error) => {
          console.log(error);
        }
      );
    } catch (err) {
      console.log(err);
    }
  }

  //Validar que las contrasenias sean iguales antes de realizar el cambio
  validatePasswords() {
    if (this.password == this.confirma) {
      return true;
    }
    return false;
  }

  reset() {
    this.alerta = 0;
  }
}
