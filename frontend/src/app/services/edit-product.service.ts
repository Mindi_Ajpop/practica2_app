import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class EditProductService {
  constructor(private http: HttpClient) {}

  getProductos(): Observable<any> {
    let url = `http://${environment.api}:3001/producto/read`;
    return this.http.get(url);
  }

  getRestaurantes(): Observable<any> {
    let url = `http://${environment.api}:3001/restaurante/info/get`;
    return this.http.get(url);
  }

  deleteProduct(id): Observable<any> {
    let url = `http://${environment.api}:3001/producto/` + id;
    const headerss2 = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'content-type': 'application/json',
    });
    return this.http.delete(url, { headers: headerss2 });
  }

  getProduct(idProducto: any): Observable<any> {
    const headerss2 = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'content-type': 'application/json',
    });

    let api2 = `http://${environment.api}:3001/producto/` + idProducto;
    return this.http.get(api2, { headers: headerss2 });
  }

  updateProduct(Producto: {}): any {
    const headerss = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'content-type': 'application/json',
    });

    let api = `http://${environment.api}:3001/producto/update/`;

    return this.http.put(
      api,
      {
        id_producto: Producto['id_producto'],
        nombre: Producto['nombre'],
        precio: Producto['precio'],
        fotografia: Producto['fotografia'],
      },
      { headers: headerss }
    );
  }
}
