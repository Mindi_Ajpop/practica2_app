import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class OrdenesService {
  URI = `http://${environment.api}:3001/`;
  constructor(private http: HttpClient) {}

  newOrden(orden: any, id): Observable<any> {
    return this.http.post(`${this.URI}ordenes/${id}`, orden);
  }

  updateOrden(orden: any): Observable<any> {
    return this.http.put(`${this.URI}ordenes/${orden.id}`, orden);
  }

  getOrdenes(id): Observable<any> {
    return this.http.get(`${this.URI}ordenes/${id}`);
  }

  getAllOrdenes(idAdmin): Observable<any> {
    return this.http.get(`${this.URI}all/Ordenes/${idAdmin}`);
  }
}
