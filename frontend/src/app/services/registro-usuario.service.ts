import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class RegistroUsuarioService {
  URI = `http://${environment.api}:3001/usuarios/`;

  constructor(private http: HttpClient) {}

  registrarUsuario(usuario: any) {
    if (usuario.tipo == true) {
      usuario.tipo = 1;
    } else {
      usuario.tipo = 2;
    }
    console.log(usuario);
    return this.http.post(this.URI, usuario);
  }
}
