import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UpdateServiceService {
  isLoggedIn = false;
  apiServer = `http://${environment.api}:3001/`;
  constructor(private http: HttpClient) {}

  Update(id, nombre, apellido, email, dpi, direccion, password, tipo): any {
    console.log(id, nombre, apellido, email, dpi, direccion, password, tipo);
    let url = `${this.apiServer}usuarios/${id}`;
    return this.http.put(url, {
      nombre: nombre,
      apellido: apellido,
      correo: email,
      dpi: dpi,
      direccion: direccion,
      password: password,
      id: id,
      tipo: tipo,
    });
  }

  getInfo(id): any {
    let url = `${this.apiServer}usuarios/${id}`;
    return this.http.get(url);
  }

  UserLogin(User: string, Pass: string): any {
    //pendiente lo del encriptado
    this.isLoggedIn = true;
    return this.http.post(`${this.apiServer}login`, {
      Username: User,
      Password: Pass,
    });
  }
}
